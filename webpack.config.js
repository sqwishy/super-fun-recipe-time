const webpack = require('webpack');
const path = require('path');
const env = require('process').env;

const development = {
    mode: "development", // https://webpack.js.org/concepts/mode/ ?
    resolve: {
        modules: [
            "node_modules",
            path.resolve(__dirname, "node_modules")
        ],
        mainFields: ["dev:module", "module", "main"],
        alias: {
            'react': 'inferno-compat',
            'react-dom': 'inferno-compat'
        }
    }
}

const production = {
    mode: "production",
    resolve: {
        modules: [
            "node_modules",
            path.resolve(__dirname, "node_modules")
        ],
        mainFields: ["module", "main"],
        alias: {
            'react': 'inferno-compat',
            'react-dom': 'inferno-compat'
        }
    }
}

module.exports = env.PRODUCTION ? production : development;
