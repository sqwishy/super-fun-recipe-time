Super Fun Recipe Time
---------------------

This project shows a recipe for banana bread, see
https://froghat.ca/super-fun-recipe-time for a demo.

This project was created so I could try out inferno and TypeScript. It uses ninja
instead of Bower, Grunt, Brunch, etc. because it's easier to write, runs faster, you get
incremental rebuilds and parallelism for free, and I wanted to make a statement. (I'm
not good at making statements.)

You need `ninja <https://ninja-build.org/>`_, Node.Js, yarn, and :code:`sassc` available
to build this.

There are three important build targets, :code:`dev`, :code:`pro`, and :code:`test`.
Any of them will install TypeScript and things in :code:`node_modules` with yarn.

The :code:`dev` and :code:`pro` targets builds the website under
:code:`build/development` and :code:`build/production` respectively. Run
:code:`ninja pro` to build that target or just :code:`ninja` for the development build;
it is the default.
Those generated files can be served by a webserver like Nginx or, for development if you
like Python, you can run :code:`python3 -m http.server` in a build directory and visit
:code:`http://localhost:8000`.

The :code:`test` target builds and runs the tests with Mocha.
