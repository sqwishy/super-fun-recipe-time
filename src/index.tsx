import { render, Component } from "inferno";
import { createElement } from "inferno-create-element";

import { Decimal } from "decimal.js-light";

import { parse_decimal, Unit, Amount, Tablespoon, Cup, Teaspoon } from "./units";

class Ingredient {
    amount: Amount;
    text?: string;
}

interface Recipe {
    name: string;
    summary: string;
    makes: string,
    time: string,
    author: string,
    steps: (Ingredient | string)[];
}

var Egg = Unit.custom({"1": "egg", "0": "eggs"})
var Banana = Unit.custom({"1": "banana", "0": "bananas"})

const BANANA_BREAD = {
    name: "Banana Bread",
    author: "Mark Bittman",
    summary: "The best banana bread is a balancing act: It requires a fair amount of fat to keep it moist and lighten the crumb; a little whole wheat flour gives it some substance. And in my opinion the result should be sweet, but not overly so. Though coconut is my favorite secret ingredient, feel free to omit it or add more nuts, raisins, or other dried fruit instead if you like. This bread keeps better than most quick breads, though it probably won’t be around too long.",
    makes: "1 loaf",
    time: "About 1 hour",
    steps: [
        { amount: Amount.new("8", Tablespoon), text: "butter, softened (plus butter for the pan)" },
        { amount: Amount.new("3/2", Cup), text: "all-purpose flour" },
        { amount: Amount.new("1/2", Cup), text: "whole wheat flour" },
        { amount: Amount.new("1", Teaspoon), text: "salt" },
        { amount: Amount.new("3/2", Teaspoon), text: "baking powder" },
        { amount: Amount.new("3/4", Cup), text: "sugar" },
        { amount: Amount.new("2", Egg) },
        { amount: Amount.new("3", Banana), text: "very ripe and mashed with a fork until smooth" },
        { amount: Amount.new("1", Teaspoon), text: "vanilla extract" },
        { amount: Amount.new("1/2", Cup), text: "chopped walnuts or pecans" },
        { amount: Amount.new("1/2", Cup), text: "shredded coconut" },
        "Heat the oven to 350°F. Grease a 9x5-inch loaf pan with butter.",
        "Mix together the dry ingredients. With a hand mixer, a whisk, or in the food processor, cream the butter and beat in the eggs and bananas. Stir this mixture into the dry ingredients, just enough to combine (it’s okay if there are lumps). Gently stir in the vanilla, nuts, and coconut.",
        "Pour the batter into the loaf pan and bake for 45 to 60 minutes, until nicely browned. A toothpick inserted in the center of the bread will come out fairly clean when done, but because of the bananas this bread will remain moister than most. Do not overcook. Cool on a rack for 15 minutes before removing from the pan.",
    ],
}

// ^^^ Data ... Display vvv

function AmountDisplay({amount}: {amount: Amount}) {
    return (<span className="amount">{amount.format()}</span>)
}

function IngredientItem (
    { scale, item }:
    { scale: Decimal, item: (Ingredient | string) }
) {
    if (typeof item  === "string") {
        return <span className="step">{item}</span>
    } else {
        let amount = item.amount.times(scale)
        let subdivision = amount.subdivide();
        let omit_subs = (subdivision.length == 1 && subdivision[0].unit == amount.unit)
        let sub_amounts = omit_subs ? [] : subdivision.map((a) => <AmountDisplay amount={a}/>);
        return (
            <div className="ingredient">
                <span className="amount-group"><AmountDisplay amount={amount}/></span>
                {(sub_amounts.length > 0) &&
                    <span className="amount-group alternate-amounts">{sub_amounts}</span>
                }
                {(item.text) &&
                    <span className="text">{item.text}</span>
                }
            </div>
            );
    }
}

class MainPageState {
    recipe: Recipe;
    summary?: Recipe;
    scale_text: string;
    scale: Decimal;
}

class MainPage extends Component<any, MainPageState> {
    constructor(props) {
        super(props);
        this.state = {
            recipe: BANANA_BREAD,
            scale_text: '',
            scale: new Decimal('1'),
        };
        this.changeScale = this.changeScale.bind(this);
    }

    changeScale(event) {
        let scale_text = event.target.value;
        let update = { scale_text: scale_text };
        if (scale_text.trim().length == 0) {
            update['scale'] = new Decimal('1')
        } else {
            try {
                update['scale'] = parse_decimal(scale_text);
            } catch (e) {
                if (console)
                    console.error("Couldn't get a reasonable number from", scale_text, e)
            }
        }
        this.setState(update)
    }

    public render() {
        const { recipe, scale_text, scale } = this.state as MainPageState;
        let items = recipe.steps.map((step) => {
            return (<li><IngredientItem scale={scale} item={step} /></li>);
        })
        return (
            <div className="recipe">
                <header>
                    <h1 className="name">{recipe.name}</h1>
                </header>
                <div className="field author">
                    <span className="name">By</span>
                    <span className="value">{recipe.author}</span>
                </div>
                <p className="summary">{recipe.summary}</p>
                <div className="field makes">
                    <span className="name">Makes</span>
                    <span className="value">{recipe.makes}</span>
                </div>
                <div className="field time">
                    <span className="name">Time</span>
                    <span className="value">{recipe.time}</span>
                </div>
                <label>
                    Ingredient Scaling:
                    <input placeholder="1" value={scale_text} onInput={this.changeScale}/>
                </label>
                <ol>
                    {items}
                </ol>
            </div>
        );
    }
}

render(<MainPage/>, document.getElementById('Website'));
