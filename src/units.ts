// Very basic elements for working with units and amounts of units.
// Some parts are very imperial specific ...

import { Decimal } from "decimal.js-light";

interface Map<T> {
    [key: string]: T;
}

export class Unit {
    constructor(
        public ratio: Decimal,
        public numberousness: Map<string>,
    ) {}

    static new(ratio: string, numberousness: Map<string>) {
        return new Unit(parse_decimal(ratio), numberousness)
    }

    static custom(numberousness: Map<string>) {
        return Unit.new("0", numberousness)  // ...
    }
}

export const Teaspoon = new Unit(new Decimal(1), {"1": "teaspoon", "0": "teaspoons"});
export const Tablespoon = new Unit(new Decimal(3), {"1": "tablespoon", "0": "tablespoons"});
export const Cup = new Unit(new Decimal(48), {"1": "cup", "0": "cups"});

export class Amount {
    constructor(
        public scalar: Decimal,
        public unit: Unit,
    ) {}

    static new(scalar: string, unit: Unit) {
        return new Amount(parse_decimal(scalar), unit)
    }

    times(scale: Decimal): Amount {
        return new Amount(this.scalar.times(scale), this.unit);
    }

    add(other: Amount): Amount {
        let scalar = this.scalar.add(other.to(this.unit).scalar)
        return new Amount(scalar, this.unit);
    }

    minus(other: Amount): Amount {
        let scalar = this.scalar.minus(other.to(this.unit).scalar)
        return new Amount(scalar, this.unit);
    }

    to(other: Unit): Amount {
        // Multiply our scalar by how many others are in one of my units?
        let scalar = this.scalar.times(this.unit.ratio.dividedBy(other.ratio))
        return new Amount(scalar, other)
    }

    format(): string {
        let {scalar, unit} = this
        scalar = scalar.todp(2)
        var unit_text = (
            unit.numberousness[scalar.toString()] || unit.numberousness["0"]
        )
        return `${scalar.toString()} ${unit_text}`
    }

    // Will try express this amount in terms of other amounts and/or multiple of smaller
    // units.
    subdivide(): Amount[] {
        var result: Amount[] = [];
        var working: Amount = this;
        var use_one_of;
        for (use_one_of of IMPERIAL_SUBDIVISIONS) {
            var other;
            for (other of use_one_of) {
                if (other instanceof Amount) {
                    // Given a specific amount, only use it if we're greater the amount
                    // and less than double ...
                    var foreign = working.to(other.unit)
                    if (foreign.scalar.gte(other.scalar)
                            && foreign.scalar.lt(other.scalar.times("2"))) {
                        result.push(other)
                        let reduce = other.to(working.unit).scalar;
                        working = new Amount(working.scalar.minus(reduce), working.unit);
                        if (working.scalar.isZero()) {
                            return result;
                        } else {
                            break
                        }
                    }
                } else if (other instanceof Unit) {
                    // How many of these units are in one of us?
                    var foreign = working.to(other)
                    let n_whole_amounts = foreign.scalar.idiv(new Decimal("1"))
                    // If it's above zero ...
                    if (n_whole_amounts.isPositive()) {
                        let take_amount = new Amount(n_whole_amounts, other)
                        result.push(take_amount)
                        working = working.minus(take_amount)
                        if (working.scalar.isZero()) {
                            return result;
                        } else {
                            break
                        }
                    }
                } else {
                    console.warn("Couldn't subdivide", this.format(), "using whatever this is", other)
                    return [this]
                }
            }
        }
        // If we've gotten here, we haven't reached a zero working amount. The rest is
        // tablespoons ...
        if (result.length > 0) {
            let last = result[result.length-1]
            if (last.unit == Teaspoon) {
                result[result.length-1] = last.add(working)
            } else {
                result.push(working.to(Teaspoon))
            }
        }
        return result;
    }

}

const IMPERIAL_SUBDIVISIONS: (Amount|Unit)[][] = [
    // This chooses only one from each of the inner lists ...
    // But they only have one element in them so it doesn't matter. :)
    [Cup],
    [Amount.new("3/4", Cup)],
    [Amount.new("1/2", Cup)],
    [Amount.new("1/3", Cup)],
    [Amount.new("1/4", Cup)],
    [Tablespoon],
    [Teaspoon],
]

// I don't really want to do this but ...
// TODO this gets the order of operations wrong ...
// TODO this should be in a different file.
export function parse_decimal(fraction: string): Decimal {
    let i: number
    if ((i = fraction.search(/\*|\//)) != -1) {
        let operator = fraction[i];
        let lh: Decimal = new Decimal(fraction.slice(0, i).trim());
        let rh: Decimal = parse_decimal(fraction.slice(i + 1));
        if (operator == "/") {
            return lh.dividedBy(rh)
        } else {
            return lh.times(rh)
        }
    } else {
        return new Decimal(fraction.trim());
    }
}
