import { describe, it } from "mocha";
import { expect } from "chai";
import { Unit, Amount, Cup, Tablespoon, Teaspoon } from"../src/units";

describe("subdivide", () => {

    it("8 tbsp is 0.5 cups", () => {
        expect(
            Amount.new("8", Tablespoon).subdivide().map((c) => c.format())
        ).to.deep.equal(["0.5 cups"])
    });

    // TODO Not sure about this one ...
    it("3/2 cups is 1 cup, 0.5 cups", () => {
        expect(
            Amount.new("3/2", Cup).subdivide().map((c) => c.format())
        ).to.deep.equal(["1 cup", "0.5 cups"])
    });

    // TODO This is actually awful ...
    it("0.9 cups is 0.75 cups, 2 tablespoons, 1.2 teaspoons", () => {
        expect(
            Amount.new("0.9", Cup).subdivide().map((c) => c.format())
        ).to.deep.equal(["0.75 cups", "2 tablespoons", "1.2 teaspoons"])
    });


    it("1.5 teaspoons is 1.5 teaspoons", () => {
        expect(
            Amount.new("1.5", Teaspoon).subdivide().map((c) => c.format())
        ).to.deep.equal(["1.5 teaspoons"])
    });


    it("4.5 potatos is an empty list?", () => {
        // This basically only works because this unit will have a zero ratio and don't
        // convert to any other units ... but it's very silly ...
        let Potato = Unit.custom({"1": "potato", "0": "potatos"});
        expect(
            Amount.new("4.5", Potato).subdivide().map((c) => c.format())
        ).to.deep.equal([])
    });
});
